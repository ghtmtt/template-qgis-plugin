# Develoment dependencies
# -----------------------

black
{% if cookiecutter.linter_py == 'Flake8' or cookiecutter.linter_py == 'both' %}
flake8-builtins>=2.2
flake8-eradicate>=1.5
flake8-isort>=6.1
flake8-qgis>=1{%- endif %}
{% if cookiecutter.linter_py == 'PyLint' or cookiecutter.linter_py == 'both' %}pylint>=3{%- endif %}
isort>=5.13
pre-commit>=3,<4
